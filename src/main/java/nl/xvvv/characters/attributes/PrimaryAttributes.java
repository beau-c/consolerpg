package nl.xvvv.characters.attributes;

/**
 * Encapsulated class containing all primary attributes that a character can have
 */
public class PrimaryAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Overloaded constructor for initializing a new copy
     * @param attributes The attributes to copy
     */
    public PrimaryAttributes(PrimaryAttributes attributes) {
        this.strength = attributes.strength;
        this.dexterity = attributes.dexterity;
        this.intelligence = attributes.intelligence;
    }

    /**
     * @param other The other PrimaryAttributes object to add
     * @return The PrimaryAttributes object with values of another added onto it
     */
    public PrimaryAttributes add(PrimaryAttributes other) {
        this.strength += other.strength;
        this.dexterity += other.dexterity;
        this.intelligence += other.intelligence;
        return this;
    }

    public static final PrimaryAttributes ZERO = new PrimaryAttributes(0,0,0);
}
