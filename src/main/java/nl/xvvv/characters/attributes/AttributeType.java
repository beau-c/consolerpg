package nl.xvvv.characters.attributes;

/**
 * Represents a unique type of attribute that a character can have
 */
public enum AttributeType {
    STRENGTH,
    DEXTERITY,
    INTELLIGENCE
}