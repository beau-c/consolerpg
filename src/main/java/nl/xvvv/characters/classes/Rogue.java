package nl.xvvv.characters.classes;

import nl.xvvv.characters.Character;
import nl.xvvv.characters.attributes.AttributeType;
import nl.xvvv.characters.attributes.PrimaryAttributes;
import nl.xvvv.items.armor.ArmorType;
import nl.xvvv.items.weapons.WeaponType;
import java.util.Set;

public class Rogue extends Character {
    private final static PrimaryAttributes DEFAULT_ATTRIBUTES = new PrimaryAttributes(2, 6, 1);
    private final static PrimaryAttributes LEVELUP_ATTRIBUTES = new PrimaryAttributes(1,4,1);
    private final static Set<WeaponType> ALLOWED_WEAPONS = Set.of(WeaponType.DAGGER, WeaponType.SWORD);
    private final static Set<ArmorType> ALLOWED_ARMOR = Set.of(ArmorType.LEATHER, ArmorType.MAIL);

    public Rogue(String name) {
        super(name, DEFAULT_ATTRIBUTES, LEVELUP_ATTRIBUTES);
    }

    @Override
    public AttributeType getMainAttributeType() {
        return AttributeType.DEXTERITY;
    }

    @Override
    public Set<WeaponType> getAllowedWeapons() {
        return ALLOWED_WEAPONS;
    }

    @Override
    public Set<ArmorType> getAllowedArmor() {
        return ALLOWED_ARMOR;
    }
}
