package nl.xvvv.characters.classes;

import nl.xvvv.characters.Character;
import nl.xvvv.characters.attributes.AttributeType;
import nl.xvvv.characters.attributes.PrimaryAttributes;
import nl.xvvv.items.armor.ArmorType;
import nl.xvvv.items.weapons.WeaponType;
import java.util.Set;

public class Warrior extends Character {
    private final static PrimaryAttributes DEFAULT_ATTRIBUTES = new PrimaryAttributes(5, 2, 1);
    private final static PrimaryAttributes LEVELUP_ATTRIBUTES = new PrimaryAttributes(3,2,1);
    private final static Set<WeaponType> ALLOWED_WEAPONS = Set.of(WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD);
    private final static Set<ArmorType> ALLOWED_ARMOR = Set.of(ArmorType.MAIL, ArmorType.PLATE);

    public Warrior(String name) {
        super(name, DEFAULT_ATTRIBUTES, LEVELUP_ATTRIBUTES);
    }

    @Override
    public AttributeType getMainAttributeType() {
        return AttributeType.STRENGTH;
    }

    @Override
    public Set<WeaponType> getAllowedWeapons() {
        return ALLOWED_WEAPONS;
    }

    @Override
    public Set<ArmorType> getAllowedArmor() {
        return ALLOWED_ARMOR;
    }
}
