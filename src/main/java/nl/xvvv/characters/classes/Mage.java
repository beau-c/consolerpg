package nl.xvvv.characters.classes;

import nl.xvvv.characters.Character;
import nl.xvvv.characters.attributes.AttributeType;
import nl.xvvv.characters.attributes.PrimaryAttributes;
import nl.xvvv.items.armor.ArmorType;
import nl.xvvv.items.weapons.WeaponType;
import java.util.Set;

public class Mage extends Character {
    private final static PrimaryAttributes DEFAULT_ATTRIBUTES = new PrimaryAttributes(1, 1, 8);
    private final static PrimaryAttributes LEVELUP_ATTRIBUTES = new PrimaryAttributes(1,1,5);
    private final static Set<WeaponType> ALLOWED_WEAPONS = Set.of(WeaponType.STAFF, WeaponType.WAND);
    private final static Set<ArmorType> ALLOWED_ARMOR = Set.of(ArmorType.CLOTH);

    public Mage(String name) {
        super(name, DEFAULT_ATTRIBUTES, LEVELUP_ATTRIBUTES);
    }

    @Override
    public AttributeType getMainAttributeType() {
        return AttributeType.INTELLIGENCE;
    }

    @Override
    public Set<WeaponType> getAllowedWeapons() {
        return ALLOWED_WEAPONS;
    }

    @Override
    public Set<ArmorType> getAllowedArmor() {
        return ALLOWED_ARMOR;
    }
}