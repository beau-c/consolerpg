package nl.xvvv.characters;

import nl.xvvv.characters.attributes.AttributeType;
import nl.xvvv.characters.attributes.PrimaryAttributes;
import nl.xvvv.exceptions.InvalidArmorException;
import nl.xvvv.exceptions.InvalidWeaponException;
import nl.xvvv.items.armor.Armor;
import nl.xvvv.items.Item;
import nl.xvvv.items.SlotType;
import nl.xvvv.items.armor.ArmorType;
import nl.xvvv.items.weapons.Weapon;
import nl.xvvv.items.weapons.WeaponType;
import java.util.HashMap;
import java.util.Set;

/**
 * Abstract character class
 * @author Beau C
 */
public abstract class Character {

    /**
     * @param name The name of the character
     * @param defaultAttributes The initial attributes which the character starts with
     * @param levelUpAttributes The attributes that are added when the character levels up
     */
    public Character(
            String name,
            PrimaryAttributes defaultAttributes,
            PrimaryAttributes levelUpAttributes) {
        this.name = name;
        this.level = 1;
        this.baseAttributes = new PrimaryAttributes(defaultAttributes);
        this.levelUpAttributes = new PrimaryAttributes(levelUpAttributes);
    }

    private String name;
    private int level;
    private PrimaryAttributes baseAttributes;
    private final PrimaryAttributes levelUpAttributes;
    private final HashMap<SlotType, Item> equipment = new HashMap<>();

    /**
     * @return The characters' main type of attribute
     */
    public abstract AttributeType getMainAttributeType();

    /**
     * @return A collection of weapon types that are allowed to be equipped by the character
     */
    public abstract Set<WeaponType> getAllowedWeapons();

    /**
     * @return A collection of armor types that can be equipped by the character
     */
    public abstract Set<ArmorType> getAllowedArmor();

    public int getLevel() {
        return level;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Levels up the character, increases its stats
     */
    public void levelUp() {
        this.level++;
        this.baseAttributes = this.baseAttributes.add(this.levelUpAttributes);
    }

    /**
     * Equips a single item to the character's inventory
     * @param item The item to be equipped
     * @return Boolean indicating if the action was successful
     * @throws InvalidWeaponException If a weapon is not allowed to be equipped
     * @throws InvalidArmorException If an armor item is not allowed to be equipped
     */
    public boolean equip(Item item) throws InvalidWeaponException, InvalidArmorException {

        if (item.getSlotType() == SlotType.WEAPON) {
            // Ensure the weapon can be equipped
            WeaponType wt = ((Weapon) item).getType();
            if (!getAllowedWeapons().contains(wt))
                throw new InvalidWeaponException("Weapon is not allowed to be equipped to the character class.");
            if (item.getRequiredLevel() > level)
                throw new InvalidWeaponException("Weapon level requirement is too high.");
        }
        else {
            // Ensure the armor can be equipped
            ArmorType at = ((Armor) item).getType();
            if (!getAllowedArmor().contains(at))
                throw new InvalidArmorException("Armor is not allowed to be equipped to the character class.");
            if (item.getRequiredLevel() > level)
                throw new InvalidArmorException("Armor level requirement is too high.");
        }

        // Store the item in the equipment inventory.
        equipment.put(item.getSlotType(), item);
        return true;
    }

    /**
     * @return A string containing all character stats and information
     */
    public String getStatistics() {
        final PrimaryAttributes totalAttributes = getTotalAttributes();

        StringBuilder sb;
        sb = new StringBuilder();
        sb.append("Name: ").append(this.name).append("\n");
        sb.append("Level: ").append(this.level).append("\n");
        sb.append("Strength: ").append(totalAttributes.getStrength()).append("\n");
        sb.append("Dexterity: ").append(totalAttributes.getDexterity()).append("\n");
        sb.append("Intelligence: ").append(totalAttributes.getIntelligence()).append("\n");
        sb.append("DPS: ").append(calculateDPS()).append("\n");

        return sb.toString();
    }

    /**
     * @return The DPS of a character, based on its attributes and current weapon
     */
    public double calculateDPS() {
        double weaponDps = 1.0;
        PrimaryAttributes totalAttributes = getTotalAttributes();
        AttributeType mainAttributeType = getMainAttributeType();
        double mainAttribute = 0;
        switch(mainAttributeType) {
            case STRENGTH -> mainAttribute = totalAttributes.getStrength();
            case DEXTERITY -> mainAttribute = totalAttributes.getDexterity();
            case INTELLIGENCE -> mainAttribute = totalAttributes.getIntelligence();
        }
        if (equipment.containsKey(SlotType.WEAPON)) {
            Weapon weapon = (Weapon)equipment.get(SlotType.WEAPON);
            weaponDps = weapon.getDPS();
        }
        return weaponDps * (1.0 + mainAttribute/100.0);
    }

    /**
     * @return The total attribute stats of the character, including bonuses
     */
    public PrimaryAttributes getTotalAttributes() {
        PrimaryAttributes totalAttributes = this.baseAttributes;
        for(Item item : equipment.values()) {
            if (item.getSlotType() != SlotType.WEAPON) {
                Armor armor = (Armor) item;
                totalAttributes = totalAttributes.add(armor.getAttributes());
            }
        }
        return totalAttributes;
    }
}
