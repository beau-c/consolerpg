package nl.xvvv.console;

import nl.xvvv.characters.Character;
import nl.xvvv.characters.classes.Mage;
import nl.xvvv.characters.classes.Ranger;
import nl.xvvv.characters.classes.Rogue;
import nl.xvvv.characters.classes.Warrior;

import java.util.Scanner;

public class Game {

    public void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose your class.");
        System.out.println("1: Mage");
        System.out.println("2: Ranger");
        System.out.println("3: Rogue");
        System.out.println("4: Warrior");

        int selectedCharacter = selectCharacter(scanner);
        String name = enterName(scanner);
        Character player;
        switch(selectedCharacter) {
            case 1 -> player = new Mage(name);
            case 2 -> player = new Ranger(name);
            case 3 -> player = new Rogue(name);
            case 4 -> player = new Warrior(name);
        }

        System.out.println("Welcome, " + name + ".");
    }

    public int selectCharacter(Scanner scanner) {
        int characterSelection = -1;

        while(characterSelection == -1) {
            switch (scanner.nextInt()) {
                case 1 -> {
                    System.out.println("You picked Mage.");
                    characterSelection = 1;
                }
                case 2 -> {
                    System.out.println("You picked Ranger.");
                    characterSelection = 2;
                }
                case 3 -> {
                    System.out.println("You picked Rogue.");
                    characterSelection = 3;
                }
                case 4 -> {
                    System.out.println("You picked Warrior.");
                    characterSelection = 4;
                }
                default -> System.out.println("Please pick an option from 1 to 4.");
            }
        }
        return characterSelection;
    }

    public String enterName(Scanner scanner) {
        String name = null;
        System.out.print("Please enter your name: ");
        while(name == null) {
            name = scanner.next();
        }
        return name;
    }
}
