package nl.xvvv.exceptions;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String error) {
        super(error);
    }
}
