package nl.xvvv;

import nl.xvvv.console.Game;

public class Main {
    public static void main(String[] args) {
        new Game().run();
    }
}