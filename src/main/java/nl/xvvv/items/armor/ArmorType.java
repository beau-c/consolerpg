package nl.xvvv.items.armor;

/**
 * Represents a unique type of armor
 */
public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
