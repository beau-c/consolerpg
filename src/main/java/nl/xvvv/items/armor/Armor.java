package nl.xvvv.items.armor;

import nl.xvvv.characters.attributes.PrimaryAttributes;
import nl.xvvv.items.Item;
import nl.xvvv.items.SlotType;

public class Armor extends Item {
    /**
     * @param name Name of the armor item
     * @param requiredLevel The level that is required to equip this armor
     * @param slot // The slot in which this armor should be
     * @param type // The unique type of armor
     * @param attributes // The attributes this piece of armor has
     */
    public Armor(String name,
                 int requiredLevel,
                 SlotType slot,
                 ArmorType type,
                 PrimaryAttributes attributes) {
        super(name, requiredLevel, slot);
        this.type = type;
        this.attributes = attributes;
    }

    private final ArmorType type;
    private final PrimaryAttributes attributes;

    public ArmorType getType() {
        return type;
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }
}
