package nl.xvvv.items;

/**
 * Represents slots in which items are to be equipped
 */
public enum SlotType {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
