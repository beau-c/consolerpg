package nl.xvvv.items.weapons;

/**
 * Represents a unique type of weapon
 */
public enum WeaponType {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND
}