package nl.xvvv.items.weapons;

import nl.xvvv.items.Item;
import nl.xvvv.items.SlotType;

public class Weapon extends Item {

    /**
     * @param name The name of the weapon
     * @param requiredLevel The level that is required to equip the weapon
     * @param type The type of the weapon
     * @param damage The amount of damage the weapon deals
     * @param attackSpeed The speed in which the weapon attacks
     */
    public Weapon(String name, int requiredLevel, WeaponType type, double damage, double attackSpeed) {
        super(name, requiredLevel, SlotType.WEAPON);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    private final double damage;
    private final double attackSpeed;
    private final WeaponType type;

    /**
     * @return The type of this weapon
     */
    public WeaponType getType() {
        return this.type;
    }

    /**
     * @return The DPS of this weapon
     */
    public double getDPS() {
        return this.damage * this.attackSpeed;
    }
}
