package nl.xvvv.items;

/**
 * A single item that a character can equip.
 */
public abstract class Item {

    private final String name;
    private final int requiredLevel;
    private final SlotType slotType;

    public Item(String name, int requiredLevel, SlotType slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slotType = slot;
    }

    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public SlotType getSlotType() {
        return slotType;
    }
}
