package nl.xvvv.characters;

import nl.xvvv.characters.attributes.PrimaryAttributes;
import nl.xvvv.characters.classes.Mage;
import nl.xvvv.characters.classes.Ranger;
import nl.xvvv.characters.classes.Rogue;
import nl.xvvv.characters.classes.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    public void Test_CharacterCreated_IsLevel1() {
        Character mage = new Mage("Merlin");
        assertEquals(1, mage.getLevel());
    }

    @Test
    public void Test_LevelUp_IncreasesLevel() {
        Character character = new Mage("Merlin");
        character.levelUp();
        assertEquals(2, character.getLevel());
    }

    @Test
    public void Test_MageCreated_HasCorrectDefaultAttributes() {
        Mage mage = new Mage("Merlin");
        PrimaryAttributes attributes = mage.getTotalAttributes();
        assertEquals(1, attributes.getStrength());
        assertEquals(1, attributes.getDexterity());
        assertEquals(8, attributes.getIntelligence());
    }

    @Test
    public void Test_RangerCreated_HasCorrectDefaultAttributes() {
        Ranger ranger = new Ranger("Robin");
        PrimaryAttributes attributes = ranger.getTotalAttributes();
        assertEquals(attributes.getStrength(), 1);
        assertEquals(attributes.getDexterity(), 7);
        assertEquals(attributes.getIntelligence(), 1);
    }

    @Test
    public void Test_RogueCreated_HasCorrectDefaultAttributes() {
        Rogue rogue = new Rogue("Roger");
        PrimaryAttributes attributes = rogue.getTotalAttributes();
        assertEquals(attributes.getStrength(), 2);
        assertEquals(attributes.getDexterity(), 6);
        assertEquals(attributes.getIntelligence(), 1);
    }

    @Test
    public void Test_WarriorCreated_HasCorrectDefaultAttributes() {
        Warrior warrior = new Warrior("Alexander");
        PrimaryAttributes attributes = warrior.getTotalAttributes();
        assertEquals(attributes.getStrength(), 5);
        assertEquals(attributes.getDexterity(), 2);
        assertEquals(attributes.getIntelligence(), 1);
    }

    @Test
    public void Test_MageLevelUp_HasAttributesIncreased() {
        Mage mage = new Mage("Merlin");
        mage.levelUp();
        PrimaryAttributes attributes = mage.getTotalAttributes();
        assertEquals(attributes.getStrength(), 1 + 1);
        assertEquals(attributes.getDexterity(), 1 + 1);
        assertEquals(attributes.getIntelligence(), 8 + 5);
    }

    @Test
    public void Test_RangerLevelUp_HasAttributesIncreased() {
        Ranger ranger = new Ranger("Robin");
        ranger.levelUp();
        PrimaryAttributes attributes = ranger.getTotalAttributes();
        assertEquals(attributes.getStrength(), 1 + 1);
        assertEquals(attributes.getDexterity(), 7 + 5);
        assertEquals(attributes.getIntelligence(), 1 + 1);
    }

    @Test
    public void Test_RogueLevelUp_HasAttributesIncreased() {
        Rogue rogue = new Rogue("Roger");
        rogue.levelUp();
        PrimaryAttributes attributes = rogue.getTotalAttributes();
        assertEquals(attributes.getStrength(), 2 + 1);
        assertEquals(attributes.getDexterity(), 6 + 4);
        assertEquals(attributes.getIntelligence(), 1 + 1);
    }

    @Test
    public void Test_WarriorLevelUp_HasAttributesIncreased() {
        Warrior warrior = new Warrior("Alexander");
        warrior.levelUp();
        PrimaryAttributes attributes = warrior.getTotalAttributes();
        assertEquals(attributes.getStrength(), 5 + 3);
        assertEquals(attributes.getDexterity(), 2 + 2);
        assertEquals(attributes.getIntelligence(), 1 + 1);
    }

}