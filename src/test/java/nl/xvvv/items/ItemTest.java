package nl.xvvv.items;

import nl.xvvv.characters.attributes.PrimaryAttributes;
import nl.xvvv.characters.classes.Warrior;
import nl.xvvv.exceptions.InvalidArmorException;
import nl.xvvv.exceptions.InvalidWeaponException;
import nl.xvvv.items.armor.Armor;
import nl.xvvv.items.armor.ArmorType;
import nl.xvvv.items.weapons.Weapon;
import nl.xvvv.items.weapons.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    public void Test_EquipTooHighLevelWeapon_ThrowsInvalidWeaponException() {
        Warrior testWarrior = new Warrior("Ragnar");
        Weapon testAxe = new Weapon("Common Axe", 2, WeaponType.AXE, 7, 1.1);
        assertThrows(InvalidWeaponException.class, () -> testWarrior.equip(testAxe));
    }

    @Test
    public void Test_EquipTooHighLevelArmor_ThrowsInvalidArmorException() {
        Warrior testWarrior = new Warrior("Ragnar");
        Armor testPlateArmor = new Armor("Common Plate Body Armor", 2, SlotType.BODY, ArmorType.PLATE, new PrimaryAttributes(1,0,0));
        assertThrows(InvalidArmorException.class, () -> testWarrior.equip(testPlateArmor));
    }

    @Test
    public void Test_EquipWrongWeapon_ThrowsInvalidWeaponException() {
        Warrior testWarrior = new Warrior("Ragnar");
        Weapon testBow = new Weapon("Common Bow", 1, WeaponType.BOW, 12, 0.8);
        assertThrows(InvalidWeaponException.class, () -> testWarrior.equip(testBow));
    }

    @Test
    public void Test_EquipWrongArmor_ThrowsInvalidArmorException() {
        Warrior testWarrior = new Warrior("Ragnar");
        Armor testClothArmor = new Armor("Common Cloth Head Armor", 1, SlotType.HEAD, ArmorType.CLOTH, new PrimaryAttributes(0,0,5));
        assertThrows(InvalidArmorException.class, () -> testWarrior.equip(testClothArmor));
    }

    @Test
    public void Test_EquipValidWeapon_ReturnsTrue() throws InvalidArmorException, InvalidWeaponException {
        Warrior testWarrior = new Warrior("Ragnar");
        Weapon testAxe = new Weapon("Common Axe", 1, WeaponType.AXE, 7, 1.1);
        assertTrue(testWarrior.equip(testAxe));
    }

    @Test
    public void Test_EquipValidArmor_ReturnsTrue() throws InvalidArmorException, InvalidWeaponException {
        Warrior testWarrior = new Warrior("Ragnar");
        Armor testPlateArmor = new Armor("Common Plate Body Armor", 1, SlotType.BODY, ArmorType.PLATE, new PrimaryAttributes(1,0,0));
        assertTrue(testWarrior.equip(testPlateArmor));
    }

    @Test
    public void Test_DPSWithoutWeapon_ReturnsExpected() {
        Warrior testWarrior = new Warrior("Ragnar");
        assertEquals(1.0*(1.0+(5.0/100.0)), testWarrior.calculateDPS());
    }

    @Test
    public void Test_DPSWithWeapon_ReturnsExpectedDPS() throws InvalidArmorException, InvalidWeaponException {
        Warrior testWarrior = new Warrior("Ragnar");
        Weapon testAxe = new Weapon("Common Axe", 1, WeaponType.AXE, 7, 1.1);
        testWarrior.equip(testAxe);
        assertEquals((7.0*1.1)*(1.0+(5.0/100.0)), testWarrior.calculateDPS());
    }

    @Test
    public void Test_DPSWithWeaponAndArmor_ReturnsExpectedDPS() throws InvalidArmorException, InvalidWeaponException {
        Warrior testWarrior = new Warrior("Ragnar");
        Weapon testAxe = new Weapon("Common Axe", 1, WeaponType.AXE, 7, 1.1);
        Armor testPlateArmor = new Armor("Common Plate Body Armor", 1, SlotType.BODY, ArmorType.PLATE, new PrimaryAttributes(1,0,0));
        testWarrior.equip(testAxe);
        testWarrior.equip(testPlateArmor);
        assertEquals((7.0*1.1)*(1.0+((5.0+1.0)/100.0)), testWarrior.calculateDPS());
    }
}